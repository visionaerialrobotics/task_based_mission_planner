#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <fstream>

#include "pugixml.hpp"
#include "Definitions.h"
#include "Mission.h"
#include "ErrorHandler.h"
#include "VariableHandler.h"
#include "ParameterHandler.h"

class Interpreter
{
public:
  Interpreter();
  ~Interpreter();

  Mission generateMission(std::string mission_specification_file_path);

  std::string getErrors();

  bool missionStatus(std::string& status);

private:
  std::string errors_found;

  std::string formatErrors(std::string message);

  /*!************************************************************************
   *  \brief  This method checks whether parsing errors have occur.
   *  \param  result Pugi parser's result.
   *  \param  missionConfigFile File's name where the mission has
   *          been writen.
   *  \return A string with any errors found.
   *************************************************************************/
  std::string checkParsingErrors(pugi::xml_parse_result result, std::string missionConfigFile);
  /*!************************************************************************
   *  \brief  This method builds the offset_data type necesary to transform
   *          the offset given by pugixml into a correct offset.
   *  \param  data An offset_data_t memory direction.
   *  \param  missionConfigFile File's name where the mission has
   *          been writen.
   *************************************************************************/
  void buildOffsetData(offset_data_t& data, std::string missionConfigFile);
  /*!************************************************************************
   *  \brief  This method calculates the correct offset.
   *  \param  data An offset_data_t memory direction.
   *  \param  offset pugixml's offset.
   *  \return A pair structure containing the line and the column
   *          respectively where the error has been found.
   *************************************************************************/
  std::pair<int, int> getLocation(const offset_data_t& data, ptrdiff_t offset);
  /*!************************************************************************
   *  \brief  This method checks whether a file exists.
   *  \param  name File's name..
   *  \return True if the given name matchs with a file's name,
   *          false in other case.
   *************************************************************************/
  inline bool fileExists (const std::string& name);

};

#endif
