/*!*****************************************************************************
 *  \file      Mission.h
 *  \brief     Definition of all the classes used in the file
 *             Mission.cpp .
 *
 *  \author    Guillermo De Fermin
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef MISSION_H
#define MISSION_H
#include <iostream>
#include <cstdlib>
#include "pugixml.hpp"
#include "Task.h"
#include "TreeNode.h"
#include "Definitions.h"
#include "ParameterHandler.h"
#include "EventHandler.h"
#include <string>
#include <vector>

/*!***************************************************************************
 *  \class Mission
 *  \brief This class represents an abstraction of what a mission
 *         is supossed to be.
 *****************************************************************************/
class Mission
{
private:
  std::vector<Task> root_tasks;

  std::string main_root_task_name;
  Task *root_task_in_execution;
  EventHandler event_handler;

  std::vector<Task *> task_before_event;
  std::vector<std::vector<Behavior>> recurrent_behaviors_before_event;

  std::vector<Command> urgent_commands;

  Command last_command;
  Command command_before_event;
  std::vector<Command> unfinished_commands;
  bool repeat_command_before_event;

public:
  //Constructor & Destructor
  Mission();
  ~Mission();

  // Copy assignment override
  Mission& operator=(const Mission& m);
  Mission(const Mission& m);

public:
  Command nextCommand();

  /*!************************************************************************
   *  \brief  This method gets the name of the current mission.
   *  \return A string with the name of the current mission.
   *************************************************************************/
  std::string getName();

  // Returns string with errors found or empty string if no errors
  std::string check(pugi::xml_node mission_node);

  std::vector<Event> getEvents();
  bool setTaskTree(std::string root_task_name);

  std::string getCurrentRootTaskName();
  Task *getCurrentRootTask();

  void returnFromEvent();

  void reset();

  bool ended();

  void repeatCommandBeforeEvent();

private:

  bool setMainTaskTree();

};
#endif
