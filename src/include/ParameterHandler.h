/*!*****************************************************************************
 *  \file    ParameterHandler.h
 *  \brief   Definition of all the classes used in the file
 *           ParameterHandler.cpp
 *
 *  \author  Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef PARAMETER_HANDLER
#define PARAMETER_HANDLER

#include "Definitions.h"
#include <vector>
#include <string>
 /*!***************************************************************************
 *  \class ParameterHandler
 *  \brief This class gives support to the management of the parameters
 *   recieved from the topics, saving its key and its value.
 *****************************************************************************/
class ParameterHandler
{
private:
  //Table of parameters.
  static std::vector<std::pair<std::string,std::string> > table_of_parameters;

  // Disallow creating instances
  ParameterHandler();
  ~ParameterHandler();

public:
 /*!************************************************************************
  *  \brief  Search in the table the entry that has to be updated. If not
  *          found, then a new entry is created in the table.
  *  \param  entry The entry that has to be updated.
  *************************************************************************/
  static void updateTableOfParameters(std::pair<std::string, std::string> entry);
 /*!************************************************************************
  *  \brief  This function returns the vector passed as parameter as a string
  *          according to TML the task based mission planner specifications.
  *  \param  vec Vector of integers
  *  \return A string of the vector given as parameter.
  *************************************************************************/
  static std::string fromIntVectorToString(std::vector<int> vec);
 /*!************************************************************************
  *  \brief  This function returns the double passed as parameter as a string.
  *  \param  n Double number
  *  \return A string of the number given as parameter.
  *************************************************************************/
  static std::string fromNumberToString(double n);

  static bool existsInTableOfParameters(std::string par_name);

  static void reset();

  static std::pair<std::string,std::string> findInTableOfParameters(std::string par_name);

  static void removeFromTableOfParameters(std::string key);

};
#endif
