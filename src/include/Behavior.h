/*!*****************************************************************************
 *  \file      Behavior.h
 *  \brief     Definition of all the classes used in the file
 *             Behavior.cpp .
 *
 *  \author    Guillermo De Fermin
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef BEHAVIOR_H
#define BEHAVIOR_H

#include <iostream>
#include <vector>
#include <string>
#include "pugixml.hpp"
#include <yaml-cpp/yaml.h>

class Behavior
{
private:
  std::string name;
  std::string arguments;
  bool recurrent;

public:
  Behavior();
  ~Behavior();

public:
  std::string getName();
  std::string getArguments();
  bool isRecurrent();

  // Returns string with errors found or empty string if no errors
  std::string check(pugi::xml_node node);

private:
  void setArguments(YAML::Node n);

};
#endif
