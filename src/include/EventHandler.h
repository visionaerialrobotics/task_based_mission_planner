/*!*****************************************************************************
 *  \file      EventHandler.h
 *  \brief     Definition of all the classes used in the file
 *             EventHandler.cpp .
 *
 *  \author    Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

#include <iostream>
#include <cstdlib>
#include <vector>

#include "Task.h"
#include "Event.h"
/*!***************************************************************************
 *  \class EventHandler
 *  \brief This class is supossed to be able to support all the events that
 *         may occur during the execution.
 *****************************************************************************/
class EventHandler
{
private:
  std::vector<Event> active_events;
  std::vector<Event> event_vector;
  Event *active_event;

public:
  //Constructor & Destructor
  EventHandler();
  ~EventHandler();

public:
  std::vector<Event> getEvents();

  // Returns string with errors found or empty string if no errors
  std::string check(pugi::xml_node n);


private:
  bool namesAreValid(std::vector<Event> events);
};
#endif
