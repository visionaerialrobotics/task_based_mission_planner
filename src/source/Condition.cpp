#include "Condition.h"
#include <boost/regex.hpp>

Condition::Condition()
{
}

Condition::~Condition()
{
}

std::string Condition::getBelief()
{
  return belief;
}

std::string Condition::check(pugi::xml_node condition_node)
{
  std::string errors;

  pugi::xml_attribute first_attr = condition_node.first_attribute();

  if(std::string(condition_node.name()) != "condition")
  {
    errors +=
      "Unexpected tag name '" + std::string(condition_node.name()) +
      "'. Expected: 'condition'\n";
  }

  else if(std::string(first_attr.name()) != "belief")
  {
    errors +=
      "Incorrect declaration for condition. "
      "Expected: <condition belief=\"belief_expression\" />\n";
  }

  else
  {
    belief = first_attr.value();
  }

  return errors;
}
