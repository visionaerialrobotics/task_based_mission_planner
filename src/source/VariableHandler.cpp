#include "VariableHandler.h"

void VariableHandler::setVariable(std::string key, std::string value)
{
  table_of_variables[key] = value;
}

std::string VariableHandler::getVariable(std::string key)
{
  return table_of_variables[key];
}
bool VariableHandler::labelExists(std::string key)
{
  return table_of_variables.find(key) != table_of_variables.end();
}

std::vector<double> VariableHandler::pointToVector(std::string s)
{
  std::vector<double> vector_to_return;
  //Auxiliar variables.
  bool last_symbol_was_a_digit = false;
  std::string number;
  for(uint i = 0; i< s.size(); i++)
  {
    if(isdigit(s[i]) || s[i] == '.' || s[i] == '-')
    {
      last_symbol_was_a_digit = true;
      number = number + s[i];
    }
    else if(last_symbol_was_a_digit)
    {
      vector_to_return.push_back(stod(number));
      last_symbol_was_a_digit = false;
      number = "";
    }
  }
  return vector_to_return;
}

void VariableHandler::setMacroTaskName(std::string name)
{
  table_of_macro_task_names.push_back(name);
}

bool VariableHandler::macroTaskExists(std::string key)
{
  for (std::string name: table_of_macro_task_names)
  {
    if(name == key)
      return true;
  }
  return false;
}

void VariableHandler::reset()
{
  table_of_variables.clear();
  table_of_macro_task_names.clear();
}


// Initialization of static members
std::map<std::string, std::string> VariableHandler::table_of_variables;
std::vector<std::string> VariableHandler::table_of_macro_task_names;
