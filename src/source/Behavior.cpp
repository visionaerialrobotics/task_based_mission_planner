#include <stdio.h>
#include <iostream>
#include "Behavior.h"
#include <boost/regex.hpp>

Behavior::Behavior()
{
}

Behavior::~Behavior()
{
}

std::string Behavior::getName()
{
  return name;
}

void Behavior::setArguments(YAML::Node node)
{
  if(node.IsDefined())
  {
    std::stringstream ss;
    ss << node;
    arguments = ss.str();
    if(arguments == "~") arguments = "";
  }

  else
  {
    arguments = "";
  }
}

std::string Behavior::getArguments()
{
  return arguments;
}

bool Behavior::isRecurrent()
{
  return recurrent;
}

std::string Behavior::check(pugi::xml_node node)
{
  std::string errors;
  if(std::string(node.name()) != "behavior" &&
     std::string(node.name()) != "recurrent_behavior")
  {
    errors +=
      "Unexpected tag name '" + std::string(node.name()) +
      "'. Expected: 'behavior' or 'recurrent_behavior'\n";
  }

  recurrent = std::string(node.name()) == "recurrent_behavior";

  if(std::string(node.first_attribute().name()) != "name")
  {
    errors +=
      "Expected attribute 'name' not found\n";
  }
  else if(node.first_attribute().next_attribute())
  {
    errors +=
      "Unexpected attribute '" +
      std::string(node.first_attribute().next_attribute().name()) +
      "'\n";
  }
  else
  {
    name = node.first_attribute().value();
  }

  YAML::Node arg_node;
  for(pugi::xml_node child: node.children())
  {
    if(std::string(child.name()) != "argument")
    {
      errors +=
        "Unexpected tag name '" + std::string(child.name()) +
        "'. Expected: 'argument'\n";
    }
    else if((!child.first_attribute() || !child.first_attribute().next_attribute()) ||
            std::string(child.first_attribute().name()) != "name" ||
            std::string(child.first_attribute().next_attribute().name()) != "value" ||
            child.first_attribute().next_attribute().next_attribute())
    {
      errors +=
        "Incorrect argument declaration for behavior '" +
        name + "'. "
        "Expected: <argument name=\"argument_name\" value=\"argument_value\"\n";
    }
    else
    {
      arg_node[child.first_attribute().value()] = YAML::Load(child.first_attribute().next_attribute().value());
    }
  }
  setArguments(arg_node);

  return errors;
}
