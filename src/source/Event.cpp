#include <stdio.h>
#include <iostream>
#include "Event.h"

Event::Event()
{
}

Event::~Event()
{
}

std::string Event::getDescription()
{
  return description;
}

EndingStepType Event::getEndingStep()
{
  return ending_step;
}

std::vector<Condition> Event::getConditions()
{
  return conditions;
}

std::string Event::check(pugi::xml_node n)
{
  std::string errors;
  if(std::string(n.name()) != "event")
  {
    errors +=
      "Unexpected tag name '" + std::string(n.name()) +
      "'. Expected: 'event'\n";
  }

  else if(!n.first_attribute() || std::string(n.first_attribute().name()) != "name")
  {
    errors +=
      "Attribute 'name' not found in event tag\n" ;
  }

  else if(n.first_attribute().next_attribute())
  {
    errors +=
      "Unexpected attribute '" + std::string(n.first_attribute().next_attribute().name()) +
      "' in event tag\n";
  }

  else
  {
    description = n.first_attribute().value();
  }

  n = n.first_child();

  do
  {
    if(!n)
    {
      errors +=
        "Expected 'condition' tag within event '" + description +"'\n";
    }

    else if(std::string(n.name()) != "condition")
    {
      errors +=
        "Unexpected tag name '" + std::string(n.first_child().name()) +
        "' within event '" + description +
        "'. Expected: 'condition'\n";
    }

    else
    {
      Condition condition;
      errors += condition.check(n);
      conditions.push_back(condition);
    }

    n = n.next_sibling();
  }
  while(n && std::string(n.name())=="condition");

  if(!n || std::string(n.name()) != "call")
  {
    errors +=
      "Expected 'call' tag within event '" + description +"'\n";
  }

  else
  {
    if(!n.first_attribute() || std::string(n.first_attribute().name()) != "root_task")
    {
      errors +=
        "Required argument 'root_task' not found\n";
    }
    else if(n.first_attribute().next_attribute())
    {
      errors +=
        "Unexpected attribute '" + std::string(n.first_attribute().next_attribute().name()) +
        "' within 'call' tag in event '" + description +"'\n";
    }
    else
    {
      if(!VariableHandler::macroTaskExists(n.first_attribute().value()))
      {
        errors +=
          "Macro task '" + std::string(n.first_attribute().value()) + "' has not been declared\n";
      }
      else
      {
        macro_task_name = std::string(n.first_attribute().value());
      }
    }
  }

  n = n.next_sibling();
  if(!n || std::string(n.name()) != "termination")
  {
    errors +=
      "Expected 'termination' tag within event '" + description +"'\n";
  }

  else
  {
    if(!n.first_attribute() || std::string(n.first_attribute().name()) != "mode")
    {
      errors +=
        "Required argument 'mode' not found\n";
    }
    else if(n.first_attribute().next_attribute())
    {
      errors +=
        "Unexpected attribute '" + std::string(n.first_attribute().next_attribute().name()) +
        "' within 'termination' tag in event '" + description +"'\n";
    }
    else
    {
      errors += setEndingStep(n.first_attribute().value());
    }
  }

  return errors;
}

std::string Event::setEndingStep(std::string readingValue)
{
  if (readingValue==END_NEXT_TASK)
  {
    ending_step = EndingStepType::nextTask;
  }
  else if(readingValue==END_REPEAT_TASK)
  {
    ending_step = EndingStepType::repeatTask;
  }
  else if(readingValue==END_ABORT_MISSION)
  {
    ending_step = EndingStepType::abortMission;
  }
  else if(readingValue==END_END_MISSION)
  {
    ending_step = EndingStepType::endMission;
  }
  else
  {
   return "Unknown termination mode \"" + readingValue + "\".";
  }
  return "";
}

std::string Event::getMacroTask()
{
  return macro_task_name;
}
