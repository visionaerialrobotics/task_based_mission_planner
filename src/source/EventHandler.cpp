#include <stdio.h>
#include <iostream>
#include "EventHandler.h"

EventHandler::EventHandler()
{
  //Default attribute values
}

EventHandler::~EventHandler()
{
}

std::string EventHandler::check(pugi::xml_node n)
{
  std::string errors;

  if(std::string(n.name()) != "event_handling")
  {
    errors +=
      "Unexpected tag name '" + std::string(n.name()) +
      "'. Expected: 'event_handler'\n";
  }

  //Check each event individually
  for(pugi::xml_node child: n.children())
  {
    Event event;
    errors += event.check(child);
    event_vector.push_back(event);
  }

  if(!namesAreValid(event_vector))
  {
    errors +=
      "Event names cannot be repeated.\n";
  }

  return errors;
}

bool EventHandler::namesAreValid(std::vector<Event> events)
{
  for(Event event_i: events)
  {
    int n_coincidences = 0;
    for(Event event_j: events)
    {
      if(event_i.getDescription() == event_j.getDescription())
        n_coincidences++;

      if(n_coincidences > 1)
        return false;
    }
  }

  return true;
}

std::vector<Event> EventHandler::getEvents()
{
  return event_vector;
}
